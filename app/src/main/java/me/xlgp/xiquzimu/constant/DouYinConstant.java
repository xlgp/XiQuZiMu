package me.xlgp.xiquzimu.constant;

public class DouYinConstant {
    public static final String PACKAGE_NAME = "com.ss.android.ugc.aweme";

    public static final String INTENT_DY_SERVICE_ACTION = "douyin.ACTION";

    public static final String USER_NAME = PACKAGE_NAME + ":id/" + "user_name";

    public static final String USER_HEAD_VIEW = PACKAGE_NAME + ":id/" + "head_view";
}

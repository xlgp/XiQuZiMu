package me.xlgp.xiquzimu.constant;

/**
 * @author: Administrator
 * @date: 2022-02-14 14:24
 * @desc:
 */
public enum ZimuSettingsTag {

    JUZHONG(1, "剧种", "黄梅戏"),
    JUMU(2, "剧目", "天仙配"),
    XUANDUAN(3, "选段", "树上鸟儿成双对");

    private int code;
    private String tag;
    private String title;

    /**
     * @param code code
     * @param tag   tag
     * @param title 例子
     */
    ZimuSettingsTag(int code, String tag, String title) {
        this.code = code;
        this.tag = tag;
        this.title = title;
    }

    public String getTag() {
        return tag;
    }

    public int getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }
}

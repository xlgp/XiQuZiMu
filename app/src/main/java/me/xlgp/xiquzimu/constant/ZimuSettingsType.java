package me.xlgp.xiquzimu.constant;

import java.io.Serializable;

/**
 * @author: Administrator
 * @date: 2022-01-18 15:20
 * @desc:
 */
public enum ZimuSettingsType implements Serializable {
    CHANGDUAN_PREFIX(0, "唱段前缀"),
    CHANGDUAN_SUFFIX(1, "唱段后缀"),
    CHANGCI_PREFIX(2, "唱词前缀"),
    CHANGCI_SUFFIX(3, "唱词后缀"),
    ;
    private final int id;
    private final String name;

    ZimuSettingsType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

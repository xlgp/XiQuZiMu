package me.xlgp.xiquzimu.dao;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import me.xlgp.xiquzimu.model.ChangCi;

@Dao
public interface ChangCiDao extends IDao<ChangCi> {

    @Query("select * from changci where cd_id = :changDuanId order by order_id asc")
    List<ChangCi> listByChangDuanId(long changDuanId);

    @Query("delete from changci")
    void deleteAll();

    @Query("delete from changci where cd_id = :changduanId")
    void deleteByChangDuanId(Integer changduanId);

    @Query("delete from changci where cd_id in (:idList)")
    int deleteByChangDuanIdList(List<Integer> idList);
}

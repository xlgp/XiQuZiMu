package me.xlgp.xiquzimu.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Update;

import java.util.List;

@Dao
public interface IDao<T> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(T t);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<T> list);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(T t);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(List<T> t);

    @Delete
    void delete(T t);

    @Delete
    void deleteAll(List<T> list);
}

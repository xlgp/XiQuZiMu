package me.xlgp.xiquzimu.dao;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import me.xlgp.xiquzimu.model.JuZhong;

@Dao
public interface JuZhongDao extends IDao<JuZhong> {

    @Query("select * from juzhong")
    Observable<List<JuZhong>> list();

    @Query("select name from juzhong")
    List<String> listName();

    @Query("select * from juzhong where name = :name")
    JuZhong getByName(String name);
}

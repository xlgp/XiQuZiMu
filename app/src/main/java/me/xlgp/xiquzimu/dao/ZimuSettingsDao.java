package me.xlgp.xiquzimu.dao;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import me.xlgp.xiquzimu.model.ZiMuSettingsItem;

/**
 * @author: Administrator
 * @date: 2022-01-20 15:42
 * @desc:
 */
@Dao
public interface ZimuSettingsDao extends IDao<ZiMuSettingsItem> {
    @Query("select * from zimusettingsitem")
    List<ZiMuSettingsItem> list();
}

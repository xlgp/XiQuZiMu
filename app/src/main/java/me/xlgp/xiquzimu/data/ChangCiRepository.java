package me.xlgp.xiquzimu.data;

import java.util.List;

import me.xlgp.xiquzimu.dao.ChangCiDao;
import me.xlgp.xiquzimu.db.AppDatabase;
import me.xlgp.xiquzimu.model.ChangCi;

public class ChangCiRepository {


    public List<ChangCi> listByChangDuanId(int id) {
        ChangCiDao changCiDao = AppDatabase.getInstance().changCiDao();
        return changCiDao.listByChangDuanId(id);
    }

    public int deleteByChangDuanIdList(List<Integer> changDuanIdlist) {
        ChangCiDao changCiDao = AppDatabase.getInstance().changCiDao();
        return changCiDao.deleteByChangDuanIdList(changDuanIdlist);
    }
}

package me.xlgp.xiquzimu.data;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import me.xlgp.xiquzimu.retrofit.RetrofitFactory;

public class FetchGiteeRepository implements IFetchRemoteRepository {
    /**
     * 远程获取namelist
     *
     * @return Observable
     */
    public Observable<List<String>> getNameList() {
        IGiteeRepository giteeRepository = RetrofitFactory.fromGitee(IGiteeRepository.class);
        return giteeRepository.nameList();
    }

    public Observable<List<String>> changDuan(String path) {
        IGiteeRepository giteeRepository = RetrofitFactory.fromGitee(IGiteeRepository.class);
        String[] paths = path.split("/");
        return giteeRepository.changDuan(paths[1], paths[2]);
    }

    public Observable<List<String>> getDownlaodUrl() {
        IGiteeRepository giteeRepository = RetrofitFactory.downloadFromGitee(IGiteeRepository.class);
        return giteeRepository.getDownloadUrl();
    }
}

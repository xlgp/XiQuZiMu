package me.xlgp.xiquzimu.data;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import me.xlgp.xiquzimu.retrofit.RetrofitFactory;

public class FetchGithubRepository implements IFetchRemoteRepository {

    IRemoteRepository remoteRepository;

    public FetchGithubRepository() {
        remoteRepository = RetrofitFactory.fromGithub(IGithubRepository.class);
    }

    /**
     * 远程获取namelist
     *
     * @return Observable
     */
    public Observable<List<String>> getNameList() {
        return remoteRepository.nameList();
    }

    public Observable<List<String>> changDuan(String path) {
        String[] paths = path.split("/");
        return remoteRepository.changDuan(paths[1], paths[2]);
    }
}

package me.xlgp.xiquzimu.data;

import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import me.xlgp.xiquzimu.constant.ZimuSettingsType;
import me.xlgp.xiquzimu.db.AppDatabase;
import me.xlgp.xiquzimu.designpatterns.ObserverHelper;
import me.xlgp.xiquzimu.model.ZiMuSettingsItem;

/**
 * @author: Administrator
 * @date: 2022-01-20 15:42
 * @desc: 唱段与唱词配置
 */
public class ZimuSettingsRepository {

    public @NonNull Observable<Long> save(ZiMuSettingsItem item) {
        return Observable.create((ObservableOnSubscribe<Long>) emitter -> {
            long id = AppDatabase.getInstance().zimuSettingsDao().insert(item);
            emitter.onNext(id);
        }).compose(ObserverHelper.transformer());
    }

    public Observable<Boolean> delete(ZiMuSettingsItem item) {
        return Observable.create((ObservableOnSubscribe<Boolean>) emitter -> {
            AppDatabase.getInstance().zimuSettingsDao().delete(item);
            emitter.onNext(true);
        }).compose(ObserverHelper.transformer());
    }

    public List<ZiMuSettingsItem> list(@androidx.annotation.NonNull ZimuSettingsType zimuSettingsType) {
        List<ZiMuSettingsItem> list = AppDatabase.getInstance().zimuSettingsDao().list();
        if (list == null || list.isEmpty()) return list;
        return list.stream().filter(ziMuSettingsItem -> ziMuSettingsItem.getZimuSettingsType().getId() == zimuSettingsType.getId()).collect(Collectors.toList());
    }

    public Observable<List<ZiMuSettingsItem>> getChangCi() {
        return Observable.create(emitter -> {

        });
    }

    public Observable<List<ZiMuSettingsItem>> get() {
        return Observable.create((ObservableOnSubscribe<List<ZiMuSettingsItem>>) emitter -> emitter.onNext(AppDatabase.getInstance().zimuSettingsDao().list())).compose(ObserverHelper.transformer());
    }
}

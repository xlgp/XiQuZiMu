package me.xlgp.xiquzimu.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.Calendar;

import me.xlgp.xiquzimu.constant.AppConstant;
import me.xlgp.xiquzimu.dao.ChangCiDao;
import me.xlgp.xiquzimu.dao.ChangDuanDao;
import me.xlgp.xiquzimu.dao.JuZhongDao;
import me.xlgp.xiquzimu.dao.ZimuSettingsDao;
import me.xlgp.xiquzimu.model.ChangCi;
import me.xlgp.xiquzimu.model.ChangDuan;
import me.xlgp.xiquzimu.model.JuZhong;
import me.xlgp.xiquzimu.model.ZiMuSettingsItem;

@Database(entities = {ChangCi.class, ChangDuan.class, JuZhong.class, ZiMuSettingsItem.class}, version = 4)
public abstract class AppDatabase extends RoomDatabase {

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(1970, 1, 1);
            database.execSQL("ALTER TABLE changduan ADD COLUMN createTime INTEGER not null default " + calendar.getTime().getTime());
        }
    };
    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE changduan ADD COLUMN beiZhu VARCHAR(30)");
            database.execSQL("alter table changci add column orderId integer not null default 0");
        }
    };
    private static AppDatabase instance;

    public static AppDatabase getInstance() {
        return instance;
    }

    public static void build(Context context) {
        if (instance == null) {
            synchronized (AppDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, AppConstant.SQLITE_DB_NAME)
                            .fallbackToDestructiveMigration()
                            .addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                            .build();
                }
            }
        }
    }

    public abstract ChangCiDao changCiDao();

    public abstract ChangDuanDao changDuanDao();

    public abstract JuZhongDao juZhongDao();

    public abstract ZimuSettingsDao zimuSettingsDao();
}

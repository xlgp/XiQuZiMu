package me.xlgp.xiquzimu.exception;

public class NoAccessibilityServiceException extends RuntimeException {
    public NoAccessibilityServiceException(String msg) {
        super(msg);
    }
}

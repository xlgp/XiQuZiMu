package me.xlgp.xiquzimu.listener;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * @author: Administrator
 * @date: 2022-02-14 14:18
 * @desc:
 */
public class SettingsTextWatcher implements TextWatcher {

    private int endIndex = -1; //当前选中的flag终点
    private final char first; //起始字符
    private final char end; //结尾字符

    public SettingsTextWatcher(char first, char end) {
        this.first = first;
        this.end = end;
    }

    private int firstFlagIndex(CharSequence s, int endIndex) {
        for (int i = endIndex - 1; i >= 0; i--) {
            if (s.charAt(i) == first) return i;
        }
        return -1;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        if (count == 1) {
            if (s.charAt(start) == end) { //删除
                endIndex = start;
            }
        }
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
//        if (count > 0){ //新增特殊詞組
//            if (s.charAt(start) == first && s.charAt(start+count) == end){
//
//            }
//        }
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (endIndex > -1) {
            int en = endIndex;
            endIndex = -1;
            int firstIndex = firstFlagIndex(s, en);
            s.delete(firstIndex, en);
        }
    }
}

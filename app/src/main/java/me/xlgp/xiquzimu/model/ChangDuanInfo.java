package me.xlgp.xiquzimu.model;

import java.util.ArrayList;
import java.util.List;

public class ChangDuanInfo {
    private ChangDuan changDuan;
    private List<ChangCi> changCiList;

    public ChangDuanInfo() {
        changCiList = new ArrayList<>();
        changDuan = new ChangDuan();
    }

    public ChangDuan getChangDuan() {
        return changDuan;
    }

    public void setChangDuan(ChangDuan changDuan) {
        this.changDuan = changDuan;
    }

    public List<ChangCi> getChangCiList() {
        return changCiList;
    }

    public void setChangCiList(List<ChangCi> changCiList) {
        this.changCiList = changCiList;
    }
}

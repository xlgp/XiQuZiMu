package me.xlgp.xiquzimu.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity
public class JuZhong {
    @PrimaryKey(autoGenerate = true)
    private Integer id;
    private String name;
    private long createTime;
    private long updateTime;

    public static JuZhong create() {
        JuZhong juZhong = new JuZhong();
        juZhong.setUpdateTime(new Date().getTime());
        juZhong.setCreateTime(new Date().getTime());
        return juZhong;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }
}

package me.xlgp.xiquzimu.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;

import me.xlgp.xiquzimu.util.ZimuSettingsTypeConverter;

/**
 * @author: Administrator
 * @date: 2022-01-18 15:22
 * @desc:
 */
@Entity
@TypeConverters(ZimuSettingsTypeConverter.class)
public class ZiMuSettingsItem extends ZimuSettingsBindData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String content; //内容
    private int orderId; //排序
    private int delaySecond; //间隔时间（针对唱段, 单位 s）
    private String subContent; //其他描述

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getDelaySecond() {
        return delaySecond;
    }

    public void setDelaySecond(int delaySecond) {
        this.delaySecond = delaySecond;
    }

    public String getSubContent() {
        return subContent;
    }

    public void setSubContent(String subContent) {
        this.subContent = subContent;
    }
}

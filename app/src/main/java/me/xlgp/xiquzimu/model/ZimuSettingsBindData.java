package me.xlgp.xiquzimu.model;

import java.io.Serializable;

import me.xlgp.xiquzimu.constant.ZimuSettingsType;


/**
 * @author: Administrator
 * @date: 2022-01-18 15:17
 * @desc:
 */

public abstract class ZimuSettingsBindData implements Serializable {

    private ZimuSettingsType zimuSettingsType;

    public ZimuSettingsType getZimuSettingsType() {
        return zimuSettingsType;
    }

    public void setZimuSettingsType(ZimuSettingsType zimuSettingsType) {
        this.zimuSettingsType = zimuSettingsType;
    }
}

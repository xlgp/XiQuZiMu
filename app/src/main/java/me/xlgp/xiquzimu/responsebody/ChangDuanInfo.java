package me.xlgp.xiquzimu.responsebody;

import java.util.ArrayList;
import java.util.List;

/**
 * 远程获取唱段名称列表，name.list中获取
 */
public class ChangDuanInfo {
    //唱段名称
    List<String> nameList;
    //剧种
    List<String> juZhongList;

    public ChangDuanInfo() {
        nameList = new ArrayList<>();
        juZhongList = new ArrayList<>();
    }

    public void addNameList(String name) {
        nameList.add(name);
    }

    public void addJuZhong(String juZhong) {
        juZhongList.add(juZhong);
    }

    public List<String> getNameList() {
        return nameList;
    }

    public void setNameList(List<String> nameList) {
        this.nameList = nameList;
    }

    public List<String> getJuZhongList() {
        return juZhongList;
    }

    public void setJuZhongList(List<String> juZhongList) {
        this.juZhongList = juZhongList;
    }
}

package me.xlgp.xiquzimu.service;

import android.accessibilityservice.AccessibilityService;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.Objects;

import me.xlgp.xiquzimu.exception.NoAccessibilityServiceException;
import me.xlgp.xiquzimu.model.ChangCi;
import me.xlgp.xiquzimu.model.ChangCiList;
import me.xlgp.xiquzimu.util.InputMethodHelper;
import me.xlgp.xiquzimu.util.PingLunHelper;

/**
 * @author xlgp
 * @description 评论器
 * @time 2021-12-16 16:00
 */
public class PingLunner {

    //标记是否按当前唱词间隔时间
    public static Integer CURRENT_MILLIS = 0;

    private final Handler handler;
    private final PingLunCallback pingLunCallback;
    private final String TAG = PingLunner.class.getName();
    private final OnPingLunStateChangedListener onPingLunStateChangedListener;
    private Runnable runnable;
    private OnPingLunListener onPingLunListener;
    private STATE state = STATE.INITIALIZED;
    private MutableLiveData<ChangCiList> changCiListLiveData;

    public PingLunner() {
        handler = new Handler(Looper.getMainLooper());
        InputMethodHelper.show(DouYinAccessibilityService.getInstance());
        pingLunCallback = new PingLunCallback(this);
        onPingLunStateChangedListener = new PinglunStateChange();
    }

    public boolean isStopped() {
        return state == STATE.STOPPED;
    }

    private void start(long currentMillis) {
        if (state == STATE.STOPPED) { //todo 这里异常处理的不好，需要优化
            if (onPingLunListener != null) onPingLunListener.error(new Exception("评论已经结束了，请重新初始化"));
            return;
        }
        handler.removeCallbacks(runnable);
        runnable = new PinglunRunnable();
        handler.postDelayed(runnable, currentMillis);
        setState(STATE.PINGLUNNING);
    }

    private void setState(STATE state) {
        this.state = state;
        onPingLunStateChangedListener.onChange(state);
    }

    public void start() {
        ChangCi changCi = getCurrent();
        if (changCi == null) {
            stop();
            return;
        }
        start(changCi.getDelayMillis());
    }

    public void setOnPingLunListener(OnPingLunListener onPingLunListener) {
        this.onPingLunListener = onPingLunListener;
    }

    public void removeOnPingLunListener() {
        this.onPingLunListener = null;
    }

    /**
     * @param position     position
     * @param immediatable 是否立即发送
     */
    public void seekTo(int position, boolean immediatable) {
        Objects.requireNonNull(changCiListLiveData.getValue()).setCursor(position);
        if (state != STATE.PINGLUNNING) return;
        if (immediatable) {
            start(CURRENT_MILLIS);
        } else {
            start();
        }
    }

    public void seekTo(int position) {
        seekTo(position, true);
    }

    public void prepare() {
        setState(STATE.INITIALIZED);
        handler.removeCallbacks(runnable);
    }

    public boolean isPingLun() {
        Log.i(TAG, "isPingLun: " + state);
        return state == STATE.PINGLUNNING;
    }

    public boolean isInitialize() {
        return state == STATE.INITIALIZED;
    }

    public void pause() {
        setState(STATE.PAUSED);
        handler.removeCallbacks(runnable);
        Log.i(TAG, "状态: " + state);
    }

    public void stop() {
        setState(STATE.STOPPED);
        handler.removeCallbacks(runnable);
    }

    public boolean hasChangCiList() {
        if (changCiListLiveData == null || changCiListLiveData.getValue() == null) {
            stop();
            return false;
        }
        return true;
    }

    public ChangCi getCurrent() {
        if (hasChangCiList()) {
            return Objects.requireNonNull(changCiListLiveData.getValue()).current();
        }
        return null;
    }

    public int getPosition() {
        if (!hasChangCiList()) {
            return -1;
        }
        return Objects.requireNonNull(changCiListLiveData.getValue()).currentIndex();
    }

    public MutableLiveData<ChangCiList> getChangCiList() {
        if (changCiListLiveData == null) {
            changCiListLiveData = new MutableLiveData<>();
        }
        return changCiListLiveData;
    }

    public void run() {//此时评论数据框已打开；开始发送字幕，包括输入内容，再点击发送按钮
        if (state != STATE.PINGLUNNING) { //此处需要判断，因为如果不处于评论状态时，手点评论框时，手动评论时禁止自动发送字幕
            return;
        }
        if (!hasChangCiList()) {
            pingLunCallback.error(new Exception("无数据"));
            return;
        }
        AccessibilityService service = DouYinAccessibilityService.getInstance();
        if (service == null) {
            pingLunCallback.error(new NoAccessibilityServiceException("无辅助服务"));
            stop();
        }
        PingLunHelper.input(service, Objects.requireNonNull(changCiListLiveData.getValue()).next().getContent(), pingLunCallback);
    }

    public void release() {
        handler.removeCallbacks(runnable);
        changCiListLiveData = null;
        onPingLunListener = null;
    }

    private enum STATE {
        INITIALIZED, //初始状态，
        PINGLUNNING, //评论状态
        PAUSED, //暂停
        STOPPED, //结束
    }

    public interface OnPingLunListener {
        void error(Throwable e);

        void send(boolean sendSuccess);

        void complete();
    }

    public interface OnPingLunStateChangedListener {
        void onChange(STATE state);
    }

    static class PinglunStateChange implements OnPingLunStateChangedListener {
        private STATE state = STATE.INITIALIZED;

        @Override
        public void onChange(STATE state) {
            if (state == STATE.PINGLUNNING) {
                if (this.state != STATE.PINGLUNNING) {
                    InputMethodHelper.hide(DouYinAccessibilityService.getInstance());
                    this.state = state;
                }
            } else {
                InputMethodHelper.show(DouYinAccessibilityService.getInstance());
            }
        }
    }

    static class PinglunRunnable implements Runnable {

        @Override
        public void run() {
            try {
                //打开数输入框
                PingLunHelper.openInputLayout(DouYinAccessibilityService.getInstance());
            } catch (Exception e) {
                Log.e("TAG", "run: ", e);
            }
        }
    }

    public static class PingLunCallback implements OnPingLunListener {
        private final String TAG = PingLunCallback.class.getSimpleName();
        private final PingLunner pingLunner;

        public PingLunCallback(PingLunner pingLunner) {
            this.pingLunner = pingLunner;
        }

        public void next() {
            if (Objects.requireNonNull(pingLunner.changCiListLiveData.getValue()).hasNext()) {
                pingLunner.start();
            } else {
                pingLunner.stop();
                complete();
            }
        }

        @Override
        public void error(Throwable e) {
            if (pingLunner.isPingLun()) {
                next();
            }
            if (pingLunner.onPingLunListener != null) pingLunner.onPingLunListener.error(e);
        }

        @Override
        public void send(boolean sendSuccess) {
            Log.i(TAG, "send: " + sendSuccess);
            if (sendSuccess) next();
            if (pingLunner.onPingLunListener != null)
                pingLunner.onPingLunListener.send(sendSuccess);
        }

        @Override
        public void complete() {
            Objects.requireNonNull(pingLunner.changCiListLiveData.getValue()).setCursor(0);
            pingLunner.prepare();
            if (pingLunner.onPingLunListener != null)
                pingLunner.onPingLunListener.complete();
        }
    }
}

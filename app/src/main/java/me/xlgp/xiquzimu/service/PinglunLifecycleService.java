package me.xlgp.xiquzimu.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleService;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import me.xlgp.xiquzimu.constant.DouYinConstant;
import me.xlgp.xiquzimu.data.ChangDuanInfoRepository;
import me.xlgp.xiquzimu.model.ZiMuInfo;

public class PinglunLifecycleService extends LifecycleService {

    public static final String CHANG_DUAN_ID = "changDuanId";
    private final String TAG = PinglunLifecycleService.class.getName();
    private int changDuanId = -1;

    private PingLunner pingLunner;

    private DouYinBroadcastReceiver douYinBroadcastReceiver;

    @Override
    public void onCreate() {
        super.onCreate();
        pingLunner = new PingLunner();
        douYinBroadcastReceiver = new DouYinBroadcastReceiver();
        registerReceiver(douYinBroadcastReceiver, douYinBroadcastReceiver.getIntentFilter());
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        if (intent != null) {
            int changDuanId = intent.getIntExtra(CHANG_DUAN_ID, -1);
            if (changDuanId > 0) {
                init(changDuanId);
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(@NonNull Intent intent) {
        super.onBind(intent);
        return new PinglunBinder();
    }

    private void init(int changDuanId) {
        //todo 现状：若评论器之前已暂停,打开新的唱段后，需要重新点击开始按钮，
        //todo:打开新的唱段后应立即评论，不应该手动点击按钮，此处需要fix
        if (this.changDuanId != changDuanId) { //若与前一个id不同，则表示是新的唱段，
            pingLunner.prepare();
            this.changDuanId = changDuanId;
            //从数据库中获取新唱段
            initChangDuanInfo(changDuanId);
        } else if (pingLunner.isStopped()) {
            pingLunner.prepare();
        }
    }

    private void initChangDuanInfo(Integer changDuanId) {
        new ChangDuanInfoRepository().getZiMuInfo(changDuanId).subscribe(new Observer<ZiMuInfo>() {
            Disposable disposable;

            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                disposable = d;
            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull ZiMuInfo ziMuInfo) {
                pingLunner.getChangCiList().postValue(ziMuInfo.getChangeCiList(0));
                pingLunner.getChangCiList().observe(PinglunLifecycleService.this, changCiList -> Log.i(TAG, "onChanged: " + changCiList.toString()));
                onComplete();
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                Toast.makeText(PinglunLifecycleService.this, e.getMessage() + " 没有唱词", Toast.LENGTH_SHORT).show();
                onComplete();
            }

            @Override
            public void onComplete() {
                if (disposable != null && !disposable.isDisposed()) {
                    disposable.dispose();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (douYinBroadcastReceiver != null) {
            unregisterReceiver(douYinBroadcastReceiver);
        }
        if (pingLunner != null) {
            pingLunner.release();
            pingLunner = null;
        }
    }

    public class PinglunBinder extends Binder {
        public PingLunner getPingLunService() {
            return PinglunLifecycleService.this.pingLunner;
        }
    }

    class DouYinBroadcastReceiver extends BroadcastReceiver {
        public IntentFilter getIntentFilter() {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(DouYinConstant.INTENT_DY_SERVICE_ACTION);
            return intentFilter;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(DouYinConstant.INTENT_DY_SERVICE_ACTION)) {
                String action = intent.getStringExtra("action");
                if ("run".equals(action)) {
                    pingLunner.run();
                }
            }
        }
    }
}

package me.xlgp.xiquzimu.ui.changci;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.snackbar.Snackbar;

import me.xlgp.xiquzimu.R;
import me.xlgp.xiquzimu.databinding.ActivityChangCiEditBinding;
import me.xlgp.xiquzimu.ui.base.BaseToolBarActivity;

public class ChangCiEditActivity extends BaseToolBarActivity {

    private ActivityChangCiEditBinding binding;
    private ChangCiEditAdapter changCiEditAdapter;
    private ChangCiEditViewModel viewModel;
    private MenuItem saveMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = ActivityChangCiEditBinding.inflate(getLayoutInflater()).getRoot();
        binding = DataBindingUtil.bind(root);

        setContentView(root);

        setTitle("编辑唱词");
        int changduanID = getIntent().getIntExtra("changduanID", -1);

        viewModel = new ViewModelProvider(this).get(ChangCiEditViewModel.class);

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        changCiEditAdapter = new ChangCiEditAdapter();

        initRecyclerView();

        viewModel.getChangDuanInfo().observe(this, changDuanInfo -> changCiEditAdapter.updateData(changDuanInfo.getChangCiList()));
        viewModel.selectedItemPosition.observe(this, position -> viewModel.updateJuZhong(position));

        viewModel.state.observe(this, s -> Snackbar.make(binding.getRoot(), s, Snackbar.LENGTH_SHORT).show());
        viewModel.saving.observe(this, b -> {
            if (saveMenuItem != null) {
                saveMenuItem.setEnabled(b);
            }
        });
        viewModel.saved.observe(this, aBoolean -> {
            //保存后退出此界面
            if (aBoolean) {
                finish();
            }
        });
        viewModel.loadData(changduanID);

        initSpinner();

        binding.add.setOnClickListener(this::add);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.changci_edit_toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.item_save) {
            if (saveMenuItem == null) {
                saveMenuItem = item;
            }
            viewModel.save();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initSpinner() {

        viewModel.juZhongList.observe(this, list -> {
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.changci_spinner_item, list
            );
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            binding.juMuSpinner.setAdapter(adapter);
        });

    }

    private void initRecyclerView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(changCiEditAdapter);
        changCiEditAdapter.setOnItemClickListener((itemView, view, data, position) -> changCiEditAdapter.removeItem(position));
    }

    public void add(View view) {
        viewModel.addChangCiItem();
        int position = changCiEditAdapter.addItem();
        binding.recyclerView.smoothScrollToPosition(position);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }
}
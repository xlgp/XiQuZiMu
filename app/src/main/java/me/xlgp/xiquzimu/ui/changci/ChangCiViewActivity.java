package me.xlgp.xiquzimu.ui.changci;

import android.app.Dialog;
import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import me.xlgp.xiquzimu.R;
import me.xlgp.xiquzimu.constant.ChangCiConstant;
import me.xlgp.xiquzimu.databinding.ActivityChangCiViewBinding;
import me.xlgp.xiquzimu.model.ChangDuanInfo;
import me.xlgp.xiquzimu.ui.base.BaseToolBarActivity;
import me.xlgp.xiquzimu.util.ChangDuanHelper;
import me.xlgp.xiquzimu.util.CopyHelper;

public class ChangCiViewActivity extends BaseToolBarActivity {

    private ActivityChangCiViewBinding binding;
    private ChangCiViewViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = ActivityChangCiViewBinding.inflate(getLayoutInflater()).getRoot();

        binding = DataBindingUtil.bind(root);

        setContentView(root);

        setTitle("查看唱词");

        viewModel = new ViewModelProvider.AndroidViewModelFactory(getApplication()).create(ChangCiViewViewModel.class);
        binding.setViewModel(viewModel);

        binding.setLifecycleOwner(this);
        ChangCiViewAdapter changCiViewAdapter = new ChangCiViewAdapter();
        binding.recyclerView.setAdapter(changCiViewAdapter);
        viewModel.getChangDuanInfoLiveData().observe(this, changDuanInfo -> changCiViewAdapter.updateData(changDuanInfo.getChangCiList()));
        viewModel.state.observe(this, s -> Snackbar.make(binding.getRoot(), s, Snackbar.LENGTH_SHORT).show());
    }

    @Override
    protected void onResume() {
        super.onResume();
        int changduanID = getIntent().getIntExtra("changduanID", -1);
        viewModel.loadData(changduanID);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.changci_view_toolbar_menu, menu);
        return true;
    }

    private void toChangCiActivity() {
        Intent intent = new Intent(this, ChangCiEditActivity.class);
        ChangDuanInfo changDuanInfo = viewModel.getChangDuanInfoLiveData().getValue();
        intent.putExtra("changduanID", Objects.requireNonNull(changDuanInfo).getChangDuan().getId());
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.item_copy) {
            createDialog(ChangCiConstant.SELECT_ITEM_ENMU.COPY, "选择复制类型").show();
        } else if (item.getItemId() == R.id.item_share) {
            createDialog(ChangCiConstant.SELECT_ITEM_ENMU.SHARE, "选择分享类型").show();
        } else if (item.getItemId() == R.id.item_edit) {
            toChangCiActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    private String getCopyText(ChangCiConstant.CHANGCI_TYPE_ENMU enmu) {
        ChangDuanInfo changDuanInfo = viewModel.getChangDuanInfoLiveData().getValue();
        if (changDuanInfo == null) {
            return "";
        }
        switch (enmu) {
            case CHUNJING:
                return ChangDuanHelper.copyChunJingFromChangDuanInfo(changDuanInfo).toString();
            case ZIMU:
                return ChangDuanHelper.copyFromChangDuanInfo(changDuanInfo).toString();
            default:
                return "";
        }
    }

    /**
     * 复制唱词
     *
     * @param enmu 表示类型，纯净版，字幕版
     */
    public void copy(ChangCiConstant.CHANGCI_TYPE_ENMU enmu) {
        String text = getCopyText(enmu);
        if ("".equals(text)) {
            Toast.makeText(this, "无数据可复制", Toast.LENGTH_SHORT).show();
            return;
        }
        CopyHelper.copy(ClipData.newPlainText("唱词", text), getApplication());
        Toast.makeText(this, "已复制", Toast.LENGTH_SHORT).show();
    }

    public void share(ChangCiConstant.CHANGCI_TYPE_ENMU enmu) {
        String text = getCopyText(enmu);
        ChangDuanInfo ziMuInfo = viewModel.getChangDuanInfoLiveData().getValue();
        if (ziMuInfo == null) {
            Toast.makeText(this, "数据为空", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        sendIntent.setType("text/plain");
        Intent shareIntent = Intent.createChooser(sendIntent, null);
        startActivity(shareIntent);
    }

    public Dialog createDialog(ChangCiConstant.SELECT_ITEM_ENMU enmu, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setItems(R.array.copy_item_array, (dialog, which) -> {
                    switch (enmu) {
                        case SHARE:
                            share(ChangCiConstant.CHANGCI_TYPE_ENMU.values()[which]);
                            break;
                        case COPY:
                            copy(ChangCiConstant.CHANGCI_TYPE_ENMU.values()[which]);
                    }
                    dialog.dismiss();
                });
        return builder.create();
    }
}
package me.xlgp.xiquzimu.ui.changci;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import me.xlgp.xiquzimu.R;
import me.xlgp.xiquzimu.adapter.BaseAdapter;
import me.xlgp.xiquzimu.databinding.ChangCiItemViewBinding;
import me.xlgp.xiquzimu.model.ChangCi;

public class ChangCiViewAdapter extends BaseAdapter<ChangCi> {
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ChangCiItemViewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.chang_ci_item_view, parent, false);
        return new ViewHolder(binding);
    }

    static class ViewHolder extends BaseAdapter.ViewHolder<ChangCi> {
        private ChangCiItemViewBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public ViewHolder(ChangCiItemViewBinding binding) {
            this(binding.getRoot());
            this.binding = binding;
        }

        @Override
        public void setData(ChangCi data) {
            super.setData(data);
            binding.setChangci(data);
            binding.executePendingBindings();
        }
    }
}

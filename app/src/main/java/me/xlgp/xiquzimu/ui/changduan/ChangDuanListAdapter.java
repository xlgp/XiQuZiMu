package me.xlgp.xiquzimu.ui.changduan;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import me.xlgp.xiquzimu.R;
import me.xlgp.xiquzimu.adapter.BaseAdapter;
import me.xlgp.xiquzimu.adapter.SearchListAdapter;
import me.xlgp.xiquzimu.databinding.ChangduanItemLayoutBinding;
import me.xlgp.xiquzimu.params.TagChangDuan;

public class ChangDuanListAdapter extends SearchListAdapter<TagChangDuan> {
    private final List<Integer> idList;
    private Boolean checkEnable;
    private Boolean isSelectAll;

    public ChangDuanListAdapter() {
        super();
        checkEnable = false;
        idList = new ArrayList<>();
        isSelectAll = false;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(getInflatedView(R.layout.changduan_item_layout, parent, false));
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setCheckEnable(Boolean checkEnable) {
        this.checkEnable = checkEnable;
        notifyDataSetChanged();
    }

    public void setCancel() {
        isSelectAll = false;
        idList.clear();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void selectAll() {
        isSelectAll = !isSelectAll;
        idList.clear();
        if (isSelectAll) {
            idList.addAll(list.stream().map(TagChangDuan::getId).collect(Collectors.toList()));
        }
        notifyDataSetChanged();
    }

    public List<Integer> getIdList() {
        return idList;
    }

    public Boolean getSelectAll() {
        return isSelectAll;
    }

    protected class ViewHolder extends BaseAdapter.ViewHolder<TagChangDuan> {
        private final ChangduanItemLayoutBinding binding;

        public ViewHolder(View view) {
            super(view);
            binding = ChangduanItemLayoutBinding.bind(view);
            view.setOnClickListener(v -> {
                if (!checkEnable) { //非选择状态
                    onItemClickListener.onItemClick(view, view, data, getAdapterPosition());
                } else {//选择状态
                    binding.checkBox.setChecked(!binding.checkBox.isChecked());
                }
            });
            binding.checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (!isChecked && idList.contains(data.getId())) {
                    idList.remove(data.getId());
                } else if (isChecked && !idList.contains(data.getId())) {
                    idList.add(data.getId());
                }
            });
        }

        @Override
        public void setData(TagChangDuan changDuan) {
            super.setData(changDuan);
            binding.checkBox.setChecked(isSelectAll);
            binding.checkBox.setVisibility(checkEnable ? View.VISIBLE : View.GONE);
            String title = changDuan.getJuZhong() + " " + changDuan.getJuMu();
            binding.title.setText(title);
            binding.content.setText(changDuan.getName());
        }
    }
}

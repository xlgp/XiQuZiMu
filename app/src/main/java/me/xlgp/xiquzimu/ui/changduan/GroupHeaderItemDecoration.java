package me.xlgp.xiquzimu.ui.changduan;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import me.xlgp.xiquzimu.params.TagChangDuan;

public class GroupHeaderItemDecoration extends RecyclerView.ItemDecoration {
    private final ChangDuanListAdapter changDuanListAdapter;
    private final int groupHeaderHeight = 60;

    public GroupHeaderItemDecoration(ChangDuanListAdapter changDuanListAdapter) {
        this.changDuanListAdapter = changDuanListAdapter;
    }

    private String getTagLine(int position) {
        List<TagChangDuan> list = changDuanListAdapter.getList();
        try {
            if (list != null && !list.isEmpty() &&
                    (position == 0 || !list.get(position).getTag().equals(list.get(position - 1).getTag()))) {
                return list.get(position).getTag();
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int position = parent.getChildAdapterPosition(view);

        //ItemView的position==0 或者 当前ItemView的data的tag和上一个ItemView的不相等，则为当前ItemView设置top 偏移量
        if (getTagLine(position) != null) {
            outRect.set(0, groupHeaderHeight, 0, 0);
        }
    }

    @Override
    public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.onDraw(c, parent, state);
        for (int i = 0; i < parent.getChildCount(); i++) {
            View view = parent.getChildAt(i);
            int position = parent.getChildAdapterPosition(view);
            String tag = getTagLine(position);
            if (tag != null) {
                drawGroupHeader(c, parent, view, tag);
            }
        }
    }

    @Override
    public void onDrawOver(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.onDrawOver(c, parent, state);
    }

    private void drawGroupHeader(Canvas c, RecyclerView parent, View view, String tag) {
        RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) view.getLayoutParams();
        int left = parent.getPaddingLeft();
        int bottom = view.getTop() - params.topMargin;
        int right = parent.getWidth() - parent.getPaddingRight();
        int top = bottom - groupHeaderHeight;

        Paint mPaint = new Paint();
        mPaint.setARGB(255, 245, 245, 245);
        c.drawRect(left, top, right, bottom, mPaint);

        Paint mTextPaint = new TextPaint();
        mTextPaint.setTextSize(28);
        mTextPaint.setARGB(225, 100, 100, 100);
        int x = left + 30;
        int y = top + 40;
        c.drawText(tag, x, y, mTextPaint);
    }
}
package me.xlgp.xiquzimu.ui.settings;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import java.util.Objects;

import me.xlgp.xiquzimu.constant.AppConstant;
import me.xlgp.xiquzimu.databinding.ActivityChangduanSettingsEditBinding;
import me.xlgp.xiquzimu.listener.SettingsTextWatcher;
import me.xlgp.xiquzimu.model.ZimuSettingsBindData;
import me.xlgp.xiquzimu.ui.base.BaseToolBarActivity;
import me.xlgp.xiquzimu.util.ZimuSettingsTagHelper;

public class ChangduanSettingsEditActivity extends BaseToolBarActivity {

    private ActivityChangduanSettingsEditBinding binding;
    private ChangduanSettingsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = ActivityChangduanSettingsEditBinding.inflate(getLayoutInflater()).getRoot();
        binding = DataBindingUtil.bind(root);
        setContentView(root);
        setTitle("唱段设置");

        viewModel = new ViewModelProvider(this).get(ChangduanSettingsViewModel.class);

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);

        ZimuSettingsBindData zimuSettingsBindData = (ZimuSettingsBindData) getIntent().getSerializableExtra("ZimuSettingsBindData");
        viewModel.loadData(zimuSettingsBindData);

        binding.save.setOnClickListener(v -> viewModel.save());

        viewModel.state.observe(this, s -> Toast.makeText(ChangduanSettingsEditActivity.this, s, Toast.LENGTH_SHORT).show());
        viewModel.success.observe(this, aBoolean -> {
            if (aBoolean) {
                ChangduanSettingsEditActivity.this.finish();
            }
        });
        SettingsTextWatcher settingsTextWatcher = new SettingsTextWatcher(AppConstant.CHAR_FIRST_8197, AppConstant.CHAR_END_8198);
        binding.editTextTextContent.addTextChangedListener(settingsTextWatcher);

        binding.juZhongBtn.setOnClickListener(this::onClick);
        binding.juMuBtn.setOnClickListener(this::onClick);
        binding.xuanDuanBtn.setOnClickListener(this::onClick);

        binding.editTextTextContent.addTextChangedListener(new ExampleTextWatcher());

    }

    public void onClick(View v) {
        Button button = (Button) v;
        int start = binding.editTextTextContent.getSelectionStart();
        Objects.requireNonNull(binding.editTextTextContent.getText()).insert(start, ZimuSettingsTagHelper.getTag(button.getText()));
    }

    class ExampleTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            binding.content.setText(ZimuSettingsTagHelper.getTagContent(s.toString()));
        }
    }
}
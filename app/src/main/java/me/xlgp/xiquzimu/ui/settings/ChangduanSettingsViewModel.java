package me.xlgp.xiquzimu.ui.settings;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.Objects;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import me.xlgp.xiquzimu.data.ZimuSettingsRepository;
import me.xlgp.xiquzimu.model.ZiMuSettingsItem;
import me.xlgp.xiquzimu.model.ZimuSettingsBindData;
import me.xlgp.xiquzimu.model.ZimuSettingsHeadItem;

/**
 * @author: Administrator
 * @date: 2022-01-18 14:41
 * @desc:
 */
public class ChangduanSettingsViewModel extends AndroidViewModel {

    private MutableLiveData<ZiMuSettingsItem> ziMuSettingsItem;
    public MutableLiveData<String> state;
    public MutableLiveData<Boolean> success; //是否成功保存

    public ChangduanSettingsViewModel(@NonNull Application application) {
        super(application);
        getZiMuSettingsItem();
        state = new MutableLiveData<>();
        success = new MutableLiveData<>();
    }

    public MutableLiveData<ZiMuSettingsItem> getZiMuSettingsItem() {
        if (ziMuSettingsItem == null) {
            ziMuSettingsItem = new MutableLiveData<>();
        }
        return ziMuSettingsItem;
    }

    public void loadData(ZimuSettingsBindData zimuSettingsBindData) {
        if (zimuSettingsBindData instanceof ZiMuSettingsItem) { //编辑
            ziMuSettingsItem.setValue((ZiMuSettingsItem) zimuSettingsBindData);
        } else if (zimuSettingsBindData instanceof ZimuSettingsHeadItem) { //新增
            ZiMuSettingsItem item = new ZiMuSettingsItem();
            item.setDelaySecond(10);
            item.setZimuSettingsType(zimuSettingsBindData.getZimuSettingsType());
            ziMuSettingsItem.setValue(item);
        }
    }

    public boolean check() {
        if (ziMuSettingsItem == null || ziMuSettingsItem.getValue() == null) {
            state.setValue("不知道哪里错了");
            return false;
        }
        if (ziMuSettingsItem.getValue().getContent() == null) {
            state.setValue("内容不能为空");
            return false;
        }
        if ("".equals(ziMuSettingsItem.getValue().getContent().trim())) {
            state.setValue("内容不能为空");
            return false;
        }
        if (ziMuSettingsItem.getValue().getDelaySecond() < 1) {
            state.setValue("时间不能小于 1s");
            return false;
        }
        return true;
    }



    public void save() {
        if (check()) {
            //todo SubContent应该设置成响应数据
            Objects.requireNonNull(ziMuSettingsItem.getValue()).setSubContent("距离上一句 " + ziMuSettingsItem.getValue().getDelaySecond() + "s 后发送");
            new ZimuSettingsRepository().save(ziMuSettingsItem.getValue()).subscribe(new Observer<Long>() {
                @Override
                public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                }

                @Override
                public void onNext(@io.reactivex.rxjava3.annotations.NonNull Long aLong) {
                    success.setValue(true);
                }

                @Override
                public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                    state.setValue(e.getMessage());
                }

                @Override
                public void onComplete() {

                }
            });
        }
    }
}

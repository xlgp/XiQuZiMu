package me.xlgp.xiquzimu.ui.settings;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import me.xlgp.xiquzimu.R;
import me.xlgp.xiquzimu.adapter.BaseAdapter;
import me.xlgp.xiquzimu.databinding.ZiMuSetttingsHeadBinding;
import me.xlgp.xiquzimu.databinding.ZiMuSetttingsItemBinding;
import me.xlgp.xiquzimu.model.ZiMuSettingsItem;
import me.xlgp.xiquzimu.model.ZimuSettingsBindData;
import me.xlgp.xiquzimu.model.ZimuSettingsHeadItem;
import me.xlgp.xiquzimu.util.ZimuSettingsTagHelper;

/**
 * @author: Administrator
 * @date: 2022-01-17 14:35
 * @desc:
 */

public class ZiMuSettginsAdapter extends BaseAdapter<ZimuSettingsBindData> {

    private final int HEAD = 0;
    private final int ITEM = 1;

    @NonNull
    @Override
    public BaseAdapter.ViewHolder<ZimuSettingsBindData> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ITEM) {
            return new ZimuSettingsItemViewHolder(getInflatedView(R.layout.zi_mu_setttings_item, parent, false));
        } else {
            return new ZimuSettingsHeadViewHolder(getInflatedView(R.layout.zi_mu_setttings_head, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder<ZimuSettingsBindData> holder, int position) {
        if (getItemViewType(position) == ITEM) {
            ZimuSettingsItemViewHolder itemViewHolder = (ZimuSettingsItemViewHolder) holder;
            itemViewHolder.setData((ZiMuSettingsItem) list.get(position));
        } else {
            ZimuSettingsHeadViewHolder headViewHolder = (ZimuSettingsHeadViewHolder) holder;
            headViewHolder.setData((ZimuSettingsHeadItem) list.get(position));
        }
        super.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) instanceof ZimuSettingsHeadItem) {
            return HEAD;
        } else if (list.get(position) instanceof ZiMuSettingsItem) {
            return ITEM;
        }
        return super.getItemViewType(position);
    }

    void deleteItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }


    abstract static class ZimuSettingsViewHolder<T> extends BaseAdapter.ViewHolder<ZimuSettingsBindData> {

        public ZimuSettingsViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
        }

    }

    class ZimuSettingsHeadViewHolder extends ZimuSettingsViewHolder<ZimuSettingsHeadItem> {
        ZiMuSetttingsHeadBinding binding;

        public ZimuSettingsHeadViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            binding = ZiMuSetttingsHeadBinding.bind(itemView);
        }

        public void setData(ZimuSettingsHeadItem data) {
            super.setData(data);
            binding.name.setText(data.getZimuSettingsType().getName());
            binding.addSettingItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(itemView, v, data, getAdapterPosition());
                }
            });
        }
    }

    class ZimuSettingsItemViewHolder extends ZimuSettingsViewHolder<ZiMuSettingsItem> {
        ZiMuSetttingsItemBinding binding;

        public ZimuSettingsItemViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = ZiMuSetttingsItemBinding.bind(itemView);
            binding.delete.setOnClickListener(v -> onItemClickListener.onItemClick(itemView, v, data, getAdapterPosition()));
            itemView.setOnClickListener(v -> onItemClickListener.onItemClick(itemView, v, data, getAdapterPosition()));
        }

        @SuppressLint("ResourceAsColor")
        public void setData(ZiMuSettingsItem data) {
            super.setData(data);
            binding.content.setText(ZimuSettingsTagHelper.getSpannableString(data.getContent(),R.color.purple_500));
            binding.orderId.setText(String.valueOf(data.getOrderId()));
            binding.subContent.setText(data.getSubContent());
        }
    }
}

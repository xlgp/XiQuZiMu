package me.xlgp.xiquzimu.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import me.xlgp.xiquzimu.R;
import me.xlgp.xiquzimu.constant.ZimuSettingsType;
import me.xlgp.xiquzimu.databinding.ActivityZiMuSettingsBinding;
import me.xlgp.xiquzimu.model.ZiMuSettingsItem;
import me.xlgp.xiquzimu.ui.base.BaseToolBarActivity;

public class ZiMuSettingsActivity extends BaseToolBarActivity {

    ActivityZiMuSettingsBinding binding;
    private ZiMuSettginsAdapter ziMuSettginsAdapter;
    private ZiMuSettingsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityZiMuSettingsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(this).get(ZiMuSettingsViewModel.class);

        setTitle("设置");

        initRecyclerView();

        viewModel.getZimuSettingsBindDataList().observe(this, zimuSettingsBindData -> ziMuSettginsAdapter.updateData(zimuSettingsBindData));

        viewModel.state.observe(this, s -> Toast.makeText(ZiMuSettingsActivity.this, s, Toast.LENGTH_SHORT).show());

        viewModel.success.observe(this, aBoolean -> {
            if (aBoolean){
                ziMuSettginsAdapter.deleteItem(viewModel.getPosition());
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.loadData();
    }

    private void initRecyclerView() {
        ziMuSettginsAdapter = new ZiMuSettginsAdapter();
        binding.recyclerView.setAdapter(ziMuSettginsAdapter);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ziMuSettginsAdapter.setOnItemClickListener((itemView, view, data, position) -> {
            if (view.getId() == R.id.addSettingItem || view.getId() == R.id.ZimuSettingsLayout) { //新增 或者 编辑 设置项
                if (data.getZimuSettingsType() == ZimuSettingsType.CHANGDUAN_PREFIX || data.getZimuSettingsType() == ZimuSettingsType.CHANGDUAN_SUFFIX) { //唱段
                    Intent intent = new Intent(this, ChangduanSettingsEditActivity.class);
                    intent.putExtra("ZimuSettingsBindData", data);
                    startActivity(intent);
                }else { //唱词
                    Toast.makeText(this, "唱词部分", Toast.LENGTH_SHORT).show();
                }
            } else if (view.getId() == R.id.delete) { //删除设置项
                createAlertDialog((ZiMuSettingsItem) data, position);
            }
        });
    }

    private void createAlertDialog(ZiMuSettingsItem data, int adapterPosition) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("删除")
                .setMessage("确认删除?")
                .setPositiveButton("确定", (dialog, which) -> viewModel.delete(data, adapterPosition))
                .setNegativeButton("取消", (dialog, which) -> {
                }).show();
    }

}
package me.xlgp.xiquzimu.ui.settings;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import me.xlgp.xiquzimu.constant.ZimuSettingsType;
import me.xlgp.xiquzimu.data.ZimuSettingsRepository;
import me.xlgp.xiquzimu.model.ZiMuSettingsItem;
import me.xlgp.xiquzimu.model.ZimuSettingsBindData;
import me.xlgp.xiquzimu.model.ZimuSettingsHeadItem;

/**
 * @author: Administrator
 * @date: 2022-01-20 16:33
 * @desc:
 */

public class ZiMuSettingsViewModel extends AndroidViewModel {

    public MutableLiveData<String> state;
    public MutableLiveData<Boolean> success;
    private final MutableLiveData<Boolean> loading;
    private MutableLiveData<List<ZimuSettingsBindData>> zimuSettingsBindDataList;
    private int position = -1;

    public ZiMuSettingsViewModel(@NonNull Application application) {
        super(application);
        state = new MutableLiveData<>();
        loading = new MutableLiveData<>();
        success = new MutableLiveData<>();
    }

    public MutableLiveData<List<ZimuSettingsBindData>> getZimuSettingsBindDataList() {
        if (zimuSettingsBindDataList == null) {
            zimuSettingsBindDataList = new MutableLiveData<>();
        }
        return zimuSettingsBindDataList;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void delete(ZiMuSettingsItem item, int position) {
        loading.setValue(true);
        setPosition(position);
        new ZimuSettingsRepository().delete(item).subscribe(new Observer<Boolean>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull Boolean b) {
                loading.setValue(false);
                state.setValue("删除成功");
                success.setValue(true);
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                state.setValue("删除失败 " + e.getMessage());
                loading.setValue(false);
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void sort(List<ZiMuSettingsItem> ziMuSettingsItems) {
        ziMuSettingsItems.sort((o1, o2) -> {
            int diff = o1.getZimuSettingsType().getId() - o2.getZimuSettingsType().getId();
            return diff == 0 ? o1.getId() - o2.getId() : diff;
        });
    }

    private List<ZimuSettingsBindData> setList(List<ZiMuSettingsItem> ziMuSettingsItems) {
        List<ZimuSettingsBindData> headList = new Data().getList();
        List<ZimuSettingsBindData> list = new ArrayList<>();

        int groupId = -1; //用于分组，（唱段前后缀，唱词前后缀）
        int orderId = 0; //每组序号
        for (ZiMuSettingsItem item : ziMuSettingsItems) {
            while (item.getZimuSettingsType().getId() > groupId) {
                list.add(headList.get(++groupId));
                orderId = 0;
            }
            item.setOrderId(++orderId);
            list.add(item);
        }

        while (headList.size() - 1 > groupId) {
            list.add(headList.get(++groupId));
        }
        return list;
    }

    public void setZimuSettingsBindDataList(List<ZiMuSettingsItem> ziMuSettingsItems) {

        if (ziMuSettingsItems == null || ziMuSettingsItems.isEmpty()) {
            zimuSettingsBindDataList.setValue(new Data().getList());
            return;
        }
        sort(ziMuSettingsItems);
        zimuSettingsBindDataList.setValue(setList(ziMuSettingsItems));
    }

    public void loadData() {
        loading.setValue(true);
        new ZimuSettingsRepository().get().subscribe(new Observer<List<ZiMuSettingsItem>>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull List<ZiMuSettingsItem> ziMuSettingsItems) {
                setZimuSettingsBindDataList(ziMuSettingsItems);
                loading.setValue(false);
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                loading.setValue(false);
                state.setValue(e.getMessage());
            }

            @Override
            public void onComplete() {
                loading.setValue(false);
            }
        });
    }
}

class Data {
    private final List<ZimuSettingsBindData> list = new ArrayList<>();

    public Data() {
        init();
    }

    public List<ZimuSettingsBindData> getList() {
        return list;
    }

    public void init() {

        ZimuSettingsHeadItem headItem = new ZimuSettingsHeadItem();
        headItem.setZimuSettingsType(ZimuSettingsType.CHANGDUAN_PREFIX);
        list.add(headItem);

        headItem = new ZimuSettingsHeadItem();
        headItem.setZimuSettingsType(ZimuSettingsType.CHANGDUAN_SUFFIX);
        list.add(headItem);

        headItem = new ZimuSettingsHeadItem();
        headItem.setZimuSettingsType(ZimuSettingsType.CHANGCI_PREFIX);
        list.add(headItem);

        headItem = new ZimuSettingsHeadItem();
        headItem.setZimuSettingsType(ZimuSettingsType.CHANGCI_SUFFIX);
        list.add(headItem);
    }
}
package me.xlgp.xiquzimu.ui.zimu.changci;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import me.xlgp.xiquzimu.adapter.BaseAdapter;
import me.xlgp.xiquzimu.databinding.ChangCiFragmentBinding;
import me.xlgp.xiquzimu.model.ChangCi;
import me.xlgp.xiquzimu.model.ChangCiList;
import me.xlgp.xiquzimu.service.PingLunner;
import me.xlgp.xiquzimu.service.PinglunLifecycleService;
import me.xlgp.xiquzimu.util.LayoutDirectionHelper;

public class ChangCiFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, Observer<ChangCiList>, BaseAdapter.OnItemClickListener<ChangCi> {

    private final String TAG = this.getClass().getName();
    private ChangCiFragmentBinding binding;
    private ChangCiAdapter changCiAdapter = null;
    private PinglunServiceConnection pinglunServiceConnection = null;
    private PingLunner pingLunner;
    private ChangCiList changCiList;

    private boolean checkAction = true; //标记是否对pingLunSwitch选择的响应，主要是初始化时不需要做出响应。

    public static ChangCiFragment newInstance() {
        return new ChangCiFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = ChangCiFragmentBinding.inflate(inflater, container, false);
        initRecyclerview();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.pingLunSwitchMaterial.setChecked(false);
        binding.pingLunSwitchMaterial.setOnCheckedChangeListener(this);
        initLayoutDirection();
    }

    @Override
    public void onStart() {
        super.onStart();
        startService();
    }

    private void initLayoutDirection() {
        String LAYOUT_DIRECTION = "layoutdirection";
        String CHANG_CI_DIRECTION = "changcidirection";
        LayoutDirectionHelper layoutDirectionHelper = new LayoutDirectionHelper(requireContext().getApplicationContext(), LAYOUT_DIRECTION, CHANG_CI_DIRECTION);
        binding.zimuDetailRecyclerview.setLayoutDirection(layoutDirectionHelper.getDirection());
        binding.currentZimuTitleTextView.setOnClickListener(v -> {
            int direction = binding.zimuDetailRecyclerview.getLayoutDirection();
            direction = direction == View.LAYOUT_DIRECTION_LTR ? View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR;
            binding.zimuDetailRecyclerview.setLayoutDirection(direction);
            layoutDirectionHelper.setDirection(direction);
        });
    }

    private void initData() {
        if (pingLunner == null) {
            Toast.makeText(this.requireContext(), "评论服务异常", Toast.LENGTH_SHORT).show();
            return;
        }
        pingLunner.getChangCiList().observe(this, this);
    }

    private void startService() {
        pinglunServiceConnection = new PinglunServiceConnection();
        requireContext().bindService(new Intent(requireContext(), PinglunLifecycleService.class), pinglunServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void initRecyclerview() {
        binding.zimuDetailRecyclerview.setLayoutManager(new LinearLayoutManager(requireContext()));

        changCiAdapter = new ChangCiAdapter();
        changCiAdapter.setOnItemClickListener(this);
        binding.zimuDetailRecyclerview.setAdapter(changCiAdapter);
    }

    private void updateTitleView(String text) {
        binding.currentZimuTitleTextView.setText(text);
    }

    private void updateRecyclerView(int position) {
        binding.zimuDetailRecyclerview.smoothScrollToPosition(position);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        pingLunner.removeOnPingLunListener();
        pingLunner.getChangCiList().removeObservers(this);
        binding = null;
        pingLunner = null;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (pinglunServiceConnection != null) {
            requireContext().unbindService(pinglunServiceConnection);
            pinglunServiceConnection = null;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.i(TAG, "getOnCheckedChangeListener: isChecked " + isChecked + "; " + checkAction);
        if (pingLunner != null) {
            if (checkAction) return;
            if (isChecked) {
                if (!pingLunner.isPingLun()) {
                    pingLunner.start();
                    changCiAdapter.hightLightItem(pingLunner.getPosition());
                }
            } else {
                if (pingLunner.isPingLun()) {
                    pingLunner.pause();
                    changCiAdapter.clearLightItem();
                }
            }
        }
    }

    @Override
    public void onChanged(ChangCiList changCiList) { //数据列表更新
        Log.i(TAG, "onChanged: " + changCiList.toString());
        changCiAdapter.updateData(changCiList);
        this.changCiList = changCiList;
        if (pingLunner.isInitialize()) { //是否初始状态，若是，则立即评论
            pingLunner.start();
        }
        //数据改变后，初始化SwitchMaterial，此时不需要响应OnCheckedChangeListener
        checkAction = true;
        binding.pingLunSwitchMaterial.setChecked(pingLunner.isPingLun());
        checkAction = false;
        updateRecyclerView(pingLunner.getPosition());
    }

    @Override
    public void onItemClick(View itemView, View view, ChangCi data, int position) {
        pingLunner.seekTo(position);
        updateRecyclerView(position);
        updateTitleView(data.getContent());
    }

    class PinglunServiceConnection implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            pingLunner = ((PinglunLifecycleService.PinglunBinder) service).getPingLunService();
            if (pingLunner != null) {
                pingLunner.setOnPingLunListener(new PingLunCallback(pingLunner));
            }
            initData();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            pingLunner = null;
        }
    }

    class PingLunCallback implements PingLunner.OnPingLunListener {
        private final PingLunner pingLunner;

        public PingLunCallback(PingLunner pingLunner) {
            this.pingLunner = pingLunner;
        }

        public void next() {
            int position = pingLunner.getPosition();
            if (position < 0 || position >= changCiList.size()) position = 0;
            updateRecyclerView(position);
            changCiAdapter.hightLightItem(position);
            updateTitleView(changCiList.get(position).getContent());
        }

        @Override
        public void error(Throwable e) {
            Toast.makeText(requireContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            next();
        }

        @Override
        public void send(boolean sendSuccess) {
            next();
        }

        @Override
        public void complete() {
            binding.pingLunSwitchMaterial.setChecked(false);
        }
    }
}
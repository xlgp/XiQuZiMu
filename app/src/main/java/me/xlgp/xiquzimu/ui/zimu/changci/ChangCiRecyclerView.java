package me.xlgp.xiquzimu.ui.zimu.changci;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import me.xlgp.xiquzimu.R;

/**
 * @author: Administrator
 * @date: 2021-12-21 10:10
 * @desc:
 */
public class ChangCiRecyclerView extends RecyclerView {
    private LinearLayoutManager linearLayoutManager;
    private int preHighPosition = -1;
    private OnChangCiOnScrollListener onChangCiOnScrollListener;

    public ChangCiRecyclerView(@NonNull Context context) {
        super(context);
        init();
    }

    public ChangCiRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ChangCiRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        onChangCiOnScrollListener = new OnChangCiOnScrollListener();
    }

    private ProgressBar getProgressBar(int position) {
        View view = getChildAt(position);
        if (view == null) {
            return null;
        }
        return view.findViewById(R.id.progressBar);
    }

    public void highItem(int position) {
        int[] positions = getFirstAndLastPosition();
        ProgressBar progressBar = getProgressBar(position);
        if (progressBar == null) return;

        if (position >= positions[0] && position <= positions[1]) {
            progressBar.setProgress(progressBar.getMax());
            preHighPosition = position;
        }
    }

    public void clearHighItem() {
        if (0 <= preHighPosition && preHighPosition <= getChildCount()) {
            ProgressBar progressBar = getProgressBar(preHighPosition);
            preHighPosition = -1;
            if (progressBar == null) return;
            progressBar.setProgress(0);
        }
    }

    public void smoothScrollToPosition(int position) {
        if (position >= 4) position += 4;
        super.smoothScrollToPosition(position);
    }

    public int[] getFirstAndLastPosition() {
        return new int[]{linearLayoutManager.findFirstVisibleItemPosition(), linearLayoutManager.findLastVisibleItemPosition()};
    }

    public void setLinearLayoutManager(LinearLayoutManager linearLayoutManager) {
        setLayoutManager(linearLayoutManager);
        this.linearLayoutManager = linearLayoutManager;
    }

    public void clear() {
        removeOnScrollListener(onChangCiOnScrollListener);
    }

    public class OnChangCiOnScrollListener extends OnScrollListener {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            ChangCiRecyclerView.this.highItem(ChangCiRecyclerView.this.preHighPosition);
        }
    }
}

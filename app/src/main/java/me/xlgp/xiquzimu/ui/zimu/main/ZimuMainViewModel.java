package me.xlgp.xiquzimu.ui.zimu.main;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import me.xlgp.xiquzimu.data.JuZhongRepository;
import me.xlgp.xiquzimu.model.JuZhong;

public class ZimuMainViewModel extends ViewModel {

    private MutableLiveData<List<String>> tabNameList;

    public MutableLiveData<List<String>> getTabNameList() {
        if (tabNameList == null) {
            tabNameList = new MutableLiveData<>();
            initTabNameList();
        }
        return tabNameList;
    }

    private void initTabNameList() {
        new JuZhongRepository().list().subscribe(new Observer<List<JuZhong>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<JuZhong> juZhongs) {
                List<String> list = juZhongs.stream().map(JuZhong::getName).collect(Collectors.toList());
                tabNameList.postValue(list);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }
}

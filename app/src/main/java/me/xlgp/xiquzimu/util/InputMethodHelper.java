package me.xlgp.xiquzimu.util;

import android.accessibilityservice.AccessibilityService;

/**
 * @author: Administrator
 * @date: 2021-12-21 14:59
 * @desc:
 */
public class InputMethodHelper {
    public static void hide(AccessibilityService service) { //此时会禁止键盘弹出
        if (service != null) {
            AccessibilityService.SoftKeyboardController controller = service.getSoftKeyboardController();
            controller.setShowMode(AccessibilityService.SHOW_MODE_HIDDEN);
        }
    }

    public static void show(AccessibilityService service) {//允许弹出键盘
        if (service != null) {
            AccessibilityService.SoftKeyboardController controller = service.getSoftKeyboardController();
            controller.setShowMode(AccessibilityService.SHOW_MODE_AUTO);
        }
    }
}

package me.xlgp.xiquzimu.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;

/**
 * @author: Administrator
 * @date: 2021-12-22 10:32
 * @desc:
 */
public class LayoutDirectionHelper {
    private final Context context;
    private final String name;
    private final String key;

    public LayoutDirectionHelper(Context context, String name, String key) {
        this.context = context;
        this.name = name;
        this.key = key;
    }

    public int getDirection() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(key, View.LAYOUT_DIRECTION_LTR);
    }

    public void setDirection(int direction) {
        SharedPreferences.Editor editor = context
                .getSharedPreferences(name, Context.MODE_PRIVATE).edit();
        editor.putInt(key, direction).apply();
    }
}

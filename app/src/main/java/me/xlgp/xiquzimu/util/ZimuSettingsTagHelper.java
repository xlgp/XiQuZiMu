package me.xlgp.xiquzimu.util;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import androidx.annotation.ColorInt;

import java.util.ArrayList;
import java.util.List;

import me.xlgp.xiquzimu.constant.AppConstant;
import me.xlgp.xiquzimu.constant.ZimuSettingsTag;
import me.xlgp.xiquzimu.model.ChangDuan;

/**
 * @author: Administrator
 * @date: 2022-03-08 16:41
 * @desc:
 */
public class ZimuSettingsTagHelper {
    public static CharSequence getTag(CharSequence tag) {
        String str = (String) tag;
        return AppConstant.CHAR_FIRST_8197 + str + AppConstant.CHAR_END_8198;
    }

    private static String getString(String str, ChangDuan changDuan) {
        if (ZimuSettingsTag.JUZHONG.getTag().equals(str)) {
            return changDuan.getJuZhong();
        } else if (ZimuSettingsTag.JUMU.getTag().equals(str)) {
            return changDuan.getJuMu();
        } else if (ZimuSettingsTag.XUANDUAN.getTag().equals(str)) {
            return changDuan.getName();
        }
        return str;
    }

    public static String getTagContent(String str, ChangDuan changDuan) {
        List<int[]> list = getTagIndexList(str);
        String content = str;
        for (int[] item : list) {
            content = content.replaceFirst(str.substring(item[0], item[1] + 1), getString(str.substring(item[0] + 1, item[1]), changDuan));
        }
        return content;
    }

    public static String getTagContent(String str) {
        List<int[]> list = getTagIndexList(str);
        String content = str;
        for (int[] item : list) {
            String tag = str.substring(item[0] + 1, item[1]);
            for (ZimuSettingsTag tagItem : ZimuSettingsTag.values()) {
                if (tagItem.getTag().equals(tag)) {
                    content = content.replaceFirst(str.substring(item[0], item[1] + 1), tagItem.getTitle());
                }
            }
        }
        return content;
    }

    private static List<int[]> getTagIndexList(String str) {
        if (str == null) return new ArrayList<>();
        if (str.indexOf(AppConstant.CHAR_FIRST_8197) == -1) {
            return new ArrayList<>();
        }
        List<int[]> list = new ArrayList<>();
        for (int i = 0; i < str.length(); ) {
            if (str.charAt(i) == AppConstant.CHAR_FIRST_8197) {
                int first = i;
                while (i < str.length() && str.charAt(i) != AppConstant.CHAR_END_8198) {
                    i++;
                }
                list.add(new int[]{first, i});
            } else {
                i++;
            }
        }
        return list;
    }

    public static CharSequence getSpannableString(String str, @ColorInt int color) {
        SpannableString spannableString = new SpannableString(str);
        List<int[]> list = getTagIndexList(str);
        list.forEach(item -> spannableString.setSpan(new ForegroundColorSpan(color), item[0], item[1], Spanned.SPAN_EXCLUSIVE_EXCLUSIVE));
        return spannableString;
    }
}

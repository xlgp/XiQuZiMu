package me.xlgp.xiquzimu.util;

import androidx.room.TypeConverter;

import com.google.gson.Gson;

import me.xlgp.xiquzimu.constant.ZimuSettingsType;

/**
 * @author: Administrator
 * @date: 2022-01-24 16:18
 * @desc:
 */
public class ZimuSettingsTypeConverter {
    @TypeConverter
    public String toString(ZimuSettingsType zimuSettingsType) {
        return new Gson().toJson(zimuSettingsType);
    }

    @TypeConverter
    public ZimuSettingsType toZimuSettingsType(String string) {
        return new Gson().fromJson(string, ZimuSettingsType.class);
    }
}

package me.xlgp.xiquzimu.view;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import me.xlgp.xiquzimu.constant.AppConstant;

//@SuppressLint("AppCompatCustomView")


/**
 * @author: Administrator
 * @date: 2022-01-25 16:27
 * @desc:
 */
public class ZimuSettingsEditText extends androidx.appcompat.widget.AppCompatEditText {

    private List<int[]> indexList;

    public ZimuSettingsEditText(Context context) {
        super(context);
        Log.i("TAG", "ZimuSettingsEditText: super(context);");
        init();
    }

    public ZimuSettingsEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.i("TAG", "ZimuSettingsEditText: super(context, attr);");
        init();
    }

    public ZimuSettingsEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Log.i("TAG", "ZimuSettingsEditText: super(context, attr,defStyleAttr);");
        init();
    }

    private void init() {
        indexList = new ArrayList<>();
    }

    @Nullable
    @Override
    public Editable getText() {
        return super.getText();
    }

    private int getEndIndex(Editable editable, int selEnd) {
        for (int i = selEnd; i < editable.length(); i++) {
            char c = editable.charAt(i);
            if (c == AppConstant.CHAR_FIRST_8197) {
                break;
            }
            if (c == AppConstant.CHAR_END_8198) {
                return i + 1;
            }
        }
        return selEnd;
    }

    private int getStartIndex(Editable editable, int selStart) {
        for (int i = selStart; i >= 0; i--) {
            char c = editable.charAt(i);
            if (c == AppConstant.CHAR_END_8198) { //需要判断前滑与后滑的区别
                if (selStart == i) return i + 1; //此处用于判断从前往后滑时情况
                break;
            }
            if (c == AppConstant.CHAR_FIRST_8197) {
                return i;
            }
        }
        return selStart;
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        super.onSelectionChanged(selStart, selEnd);

        if (getText() == null || getText().length() == 0) return;
        Editable editable = getText();

        if (selStart == selEnd) { //此时没有选择字符，需要判断光标的位置，若是在特殊词组中间，需要将光标移到特殊词组的末尾
            setSelection(getEndIndex(editable, selEnd));
        } else { //选中字符，此时光标的开头与结尾都要判断特殊词组
            setSelection(getStartIndex(editable, selStart), getEndIndex(editable, selEnd));
        }
    }
}

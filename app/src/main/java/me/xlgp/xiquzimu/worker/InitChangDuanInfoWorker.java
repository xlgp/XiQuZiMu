package me.xlgp.xiquzimu.worker;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import org.jetbrains.annotations.NotNull;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import me.xlgp.xiquzimu.data.ChangDuanInfoRepository;

public class InitChangDuanInfoWorker extends Worker {

    public InitChangDuanInfoWorker(@NonNull @NotNull Context context, @NonNull @NotNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @NotNull
    @Override
    public Result doWork() {
        if (isFirstLaunch()) {
            loadChangDuanInfo();
        }
        return Result.success();
    }

    private boolean isFirstLaunch() {
        String FIRST_LAUNCH = "first_launch";
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(FIRST_LAUNCH, Context.MODE_PRIVATE);
        String IS_FIRST = "is_first";
        boolean isFirst = sharedPreferences.getBoolean(IS_FIRST, true);
        if (isFirst) {
            sharedPreferences.edit().putBoolean(IS_FIRST, false).apply();
        }
        return isFirst;
    }

    private void loadChangDuanInfo() {
        new ChangDuanInfoRepository().fetchChangDuanList().subscribe(new ChangDuanInfoObserver());
    }

    static class ChangDuanInfoObserver implements Observer<Long> {
        private Disposable disposable;

        @Override
        public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
            disposable = d;
        }

        @Override
        public void onNext(@io.reactivex.rxjava3.annotations.NonNull Long aLong) {

        }


        @Override
        public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
            dispose();
        }

        @Override
        public void onComplete() {
            dispose();
        }

        private void dispose() {
            if (disposable != null && !disposable.isDisposed()) {
                disposable.dispose();
            }
        }
    }
}
